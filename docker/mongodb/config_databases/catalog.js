db = db.getSiblingDB('catalog_db');

db.createUser({
    user: 'dev',
    pwd: 'dev',
    roles: [
        { role: 'dbOwner', db: 'catalog_db' }
    ]
});

db.createCollection('products');