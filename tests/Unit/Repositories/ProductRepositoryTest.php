<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Services\ProductService;
use App\Models\ProductModel;
use App\Repositories\ProductRepository;

class ProductRepositoryTest extends TestCase
{
    /**
     * @var \Mockery\MockInterface
     */
    private $mock;

    public function setUp()
    {
        parent::setUp();

        $this->mock = \Mockery::mock(ProductRepository::class)->makePartial();

        $this->mock->shouldReceive('save')
        ->andReturn(true);

        $this->mock->shouldReceive('import')
            ->once()
            ->andReturn(true);

    }

    public function testRepositoryFromModel()
    {
        $dataProductModel = $this->dataProductRepository();

       $this->assertTrue($this->mock->import($dataProductModel));

    }

    private function dataProductRepository()
    {
        return [
            new ProductModel(['lm' => 1,
            'name' => 'Furadeira X',
            'category' => 'Ferramentas',
            'free_shipping' => 1,
            'description' => 'Furadeira muito boa!',
            'price' => 140.00]
            )];
    }


}
