<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Services\ProductService;
use App\GraphQL\Query\AllProductsQuery;
use App\Repositories\ProductRepository;

class AllProductsQueryTest extends TestCase
{
    /**
     * @var \Mockery\MockInterface
     */
    private $mock;

    /**
     * @var \Mockery\MockInterface
     */
    private $service;

    public function setUp()
    {
        parent::setUp();
        $this->service = \Mockery::mock(ProductService::class);
        $this->mock = new AllProductsQuery($this->service);
    }

    public function testInstanceType()
    {
        $this->assertInstanceOf('\GraphQL\Type\Definition\ListOfType',$this->mock->type());
    }


}
