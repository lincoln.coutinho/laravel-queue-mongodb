<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Services\ProductService;
use App\GraphQL\Query\ProductByIdQuery;
use App\Repositories\ProductRepository;

class ProductByIdQueryTest extends TestCase
{
    /**
     * @var \Mockery\MockInterface
     */
    private $mock;

    /**
     * @var \Mockery\MockInterface
     */
    private $service;

    public function setUp()
    {
        parent::setUp();
        $this->service = \Mockery::mock(ProductService::class);
        $this->mock = new ProductByIdQuery($this->service);
    }

    public function testInstanceType()
    {
        $this->assertInstanceOf('\GraphQL\Type\Definition\ObjectType',$this->mock->type());
    }

    public function testIsArrayArgs()
    {
        $this->assertTrue(is_array($this->mock->args()));
    }


    /**
     * @expectedException \Exception
     */
    public function testProductByIdQueryExceptionResolve()
    {

        $this->service->shouldReceive('get')
            ->once()
            ->with(1)
            ->andReturn(new ProductRepository());

        $this->mock = new ProductByIdQuery($this->service);

        $args = $this->dataProductByIdQuerynResolve();

        $this->assertInstanceOf('\App\Repositories\ProductRepository',$this->mock->resolve(null,$args));

    }

    public function dataProductByIdQuerynResolve()
    {
        return [
            'lm' => 1,
            'name' => 'Furadeira X',
            'category' => 'Ferramentas',
            'free_shipping' => 1,
            'description' => 'Furadeira muito boa!',
            'price' => 140.00
        ];
    }

}
