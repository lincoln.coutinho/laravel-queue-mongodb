<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Services\ProductService;
use App\GraphQL\Mutation\UpdateProductMutation;
use App\Repositories\ProductRepository;

class UpdateProductMutationTest extends TestCase
{
    /**
     * @var \Mockery\MockInterface
     */
    private $mock;

    /**
     * @var \Mockery\MockInterface
     */
    private $service;

    public function setUp()
    {
        parent::setUp();
        $this->service = \Mockery::mock(ProductService::class);
        $this->mock = new UpdateProductMutation($this->service);
    }

    public function testInstanceType()
    {
        $this->assertInstanceOf('\GraphQL\Type\Definition\ObjectType',$this->mock->type());
    }

    public function testIsArrayArgs()
    {
        $this->assertTrue(is_array($this->mock->args()));
    }


    /**
     * @dataProvider provideUpdateMutationArgs
     */
    public function testTypeArgs($key, $name, $class)
    {
        $instanceArgs = $this->mock->args();


        $this->assertTrue(isset($instanceArgs[$key]));
        $this->assertTrue(isset($instanceArgs[$key][$name]));
        $this->assertInstanceOf($class,$instanceArgs[$key][$name]);

    }

    public function provideUpdateMutationArgs()
    {
        return array(
            array('lm','type', '\GraphQL\Type\Definition\NonNull'),
            array('name','type', '\GraphQL\Type\Definition\NonNull'),
            array('category','type', '\GraphQL\Type\Definition\NonNull'),
            array('free_shipping','type', '\GraphQL\Type\Definition\NonNull'),
            array('description','type', '\GraphQL\Type\Definition\NonNull'),
            array('price','type', '\GraphQL\Type\Definition\NonNull'),
        );
    }


    public function testUpdateMutationResolve()
    {

        $this->service->shouldReceive('get')
            ->once()
            ->with(1)
            ->andReturn(new ProductRepository());

        $this->mock = new UpdateProductMutation($this->service);

        $args = $this->dataUpdateMutationResolve();

        $this->assertInstanceOf('\App\Repositories\ProductRepository',$this->mock->resolve(null,$args));

    }

    public function dataUpdateMutationResolve()
    {
        return [
            'lm' => 1,
            'name' => 'Furadeira X',
            'category' => 'Ferramentas',
            'free_shipping' => 1,
            'description' => 'Furadeira muito boa!',
            'price' => 140.00
        ];
    }

}
