<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About this project

This project is a test of Lincoln Coutinho for a job vacancy at Leroy Merlin.
The same is composed of the functions:

- [Mongolid](http://leroy-merlin-br.github.io/mongolid) (ODM used to intermediate transactions with the MongoDB database).
- [RabbbitMQ](https://www.rabbitmq.com/) (Messaging system and queues).
- [JsonSchemaValidator](https://github.com/justinrainbow/json-schema) (REST API request body validation).
- [GraphQL](https://graphql.org/learn) (API's language created by Facebook).
- [Mockery](http://docs.mockery.io/en/latest) (Mock Library for unit tests).
- [PHPUnit](https://phpunit.de) (API's language created by Facebook).
- [Docker](http://docker.com) (Management containers).

This project use PHP version 7.2.

## Installation

Check for processes using ports: 15672, 5672, 27017, 80, and 443. If there is any process you need to terminate it.
```
For RabbitMQ
sudo fuser -k 15672/tcp
sudo fuser -k 5672/tcp

For MongoDB
sudo fuser -k 27017/tcp

For Nginx
sudo fuser -k 80/tcp
sudo fuser -k 443/tcp
```

Insert the following line in your /etc/hosts file:

```
0.0.0.0       api.lcoutinho.intranet
```
Note: The host is purely fictitious, and is only a routing for nginx.


After inserted this line, you need to start the containers, **so you must have installed docker and docker-compose**:
```
make up
```

After this step, it is necessary running configuration command. This command will perform the necessary steps for the project to run:
 ```
docker exec app /bin/sh -c "make config"
```

Once this is done, the project should already be accessible.

## Routes

Import products

URL: http://api.lcoutinho.intranet/product/import

Method: POST

form-data

Field name: file
Field content: file (Example products.xlsx)

Expected body response:
```
{
    "status": "OK",
    "data": {
        "status": "OK"
    }
}
```

View product
URL: 

By "lm" field
http://api.lcoutinho.intranet/product/get/1001

By object id MongoDB
http://api.lcoutinho.intranet/product/get/5b6fb2083b736e16d6400293

Method: GET

Expected body response:
```
{
    "status": "OK",
    "data": {
        "_id": {
            "$oid": "5b71ccc15865193bb24b7653"
        },
        "categoryNumber": null,
        "lm": 1001,
        "name": "Furadeira X",
        "category": "Ferramentas",
        "free_shipping": 0,
        "description": "Furadeira eficiente X",
        "price": "100.00",
        "created_at": {
            "$date": {
                "$numberLong": "1534184641907"
            }
        },
        "updated_at": {
            "$date": {
                "$numberLong": "1534184641907"
            }
        }
    }
}
```

Edit product

URL: http://api.lcoutinho.intranet/product/update/{lm}

Method: PUT

Header: Content-Type:application/json

Send raw-body request:
```
{
        "lm": 1002,
        "name": "Furadeira Y",
        "category": "Ferramentas6",
        "free_shipping": 1,
        "description": "Furadeira super eficiente Y",
        "price": "140.00"
}
```
Expected body response:
```
{
    "status": "OK",
    "data": {
        "status": "OK"
    }
}
```

Delete product

URL: http://api.lcoutinho.intranet/product/delete/{lm}

Method: DELETE

Expected body response:
```
{
    "status": "OK",
    "data": {
        "status": "OK"
    }
}
```

Check status of last import

URL: http://api.lcoutinho.intranet/history/getLastStatus

Method: GET

Expected body response:
```
{
    "status": "OK",
    "data": [
        {
            "_id": {
                "$oid": "5b71ccc15865193bb24b7652"
            },
            "status": "OK",
            "filename": "/var/www/storage/app/xlsx/bec7e6557c847d6183c57610dd6aa724.xlsx",
            "dateImport": "2018-08-13 18:24:01",
            "created_at": {
                "$date": {
                    "$numberLong": "1534184641857"
                }
            },
            "updated_at": {
                "$date": {
                    "$numberLong": "1534184641857"
                }
            }
        }
    ]
}
```

## GraphQL

In this project you can also query products with GraphQL:

URL: http://api.lcoutinho.intranet/graphql?query=query{allProducts{lm,category,price}}

Method: GET

Expected body response:
```
{
	"data": {
		"allProducts": [
			{
				"lm": 1001,
				"category": "Ferramentas",
				"price": "100.00"
			},
			{
				"lm": 1002,
				"category": "Ferramentas",
				"price": "140.00"
			},
			{
				"lm": 1003,
				"category": "Ferramentas",
				"price": "20.00"
			},
			{
				"lm": 1008,
				"category": "Ferramentas",
				"price": "399.00"
			},
			{
				"lm": 1009,
				"category": "Ferramentas",
				"price": "3.90"
			},
			{
				"lm": 1010,
				"category": "Ferramentas",
				"price": "5.60"
			}
		]
	}
}
```

## RabbitMQ:

To enter the RabbitMQ interface in url: http://localhost:15672, just use the credentials:
User: dev
Password: dev

## MongoDB

It will also be possible to use the MongoDB container, with the credentials:
Host: localhost:27017
User: dev
Password: dev

## Flow

When triggering the request to import products, the api will insert into the RabbitMQ the worksheet, to be processed later (when the listen command is executed).
You can see the message in RabbitMQ, entering its interface, accessing the "Queue" tab, the "products" queue.
If a new requisition to import is made, it will accumulate in its queue.

For the queue to be consumed, you must enter the command:

```
make queue-listen
```
This command will process the queue, and will parse the worksheet, and insert the same in MongoDB.

## Docker Containers

This project is composed of 4 containers, web, app, mongodb and rabbitmq.

## Running tests

```
make test
```

Note: For some tests, connection to the bank is required.

###### No change is required in the .env file.

