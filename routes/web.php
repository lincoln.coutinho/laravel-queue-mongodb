<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'product'], function () {
    Route::get('/', 'ProductController@index')->name('product.index');
    Route::post('import', 'ProductController@store')->name('product.store');
    Route::get('get/{identify}', 'ProductController@get')->name('product.get');
    Route::put('update/{lm}', 'ProductController@update')->middleware('validator')->name('product.update');
    Route::delete('delete/{lm}', 'ProductController@destroy')->name('product.destroy');
});


Route::group(['prefix' => 'history'], function () {
    Route::get('getLastStatus', 'HistoryController@index')->name('history.index');
});
