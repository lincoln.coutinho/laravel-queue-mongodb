<?php

namespace App\Services;

use App\Models\ProductModel;
use App\Repositories\ProductRepository;
use App\Contracts\Services\ProductServiceInterface;

class ProductService implements ProductServiceInterface
{

    /**
     * @var string
     */
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Saves this object into database.
     *
     * @param \App\Repositories\ProductRepository $productModel
     * @return bool Success
     */
    public function save(ProductModel $productModel) : bool
    {
        return $this->generalAction($productModel, 'save');
    }


    /**
     * Insert this object into database.
     *
     * @param \App\Repositories\ProductRepository $productModel
     * @return bool Success
     */
    public function insert(ProductModel $productModel): bool
    {
        return $this->generalAction($productModel, 'insert');
    }

    /**
     * Updates this object in database.
     *
     * @param \App\Repositories\ProductRepository $productModel
     * @return bool Success
     */
    public function update(ProductModel $productModel): bool
    {
        return $this->generalAction($productModel, 'update');
    }

    /**
     * Deletes this object in database.
     *
     * @param \App\Repositories\ProductRepository $productModel
     * @return bool Success
     */
    public function delete(ProductModel $productModel): bool
    {
        return $this->generalAction($productModel, 'delete');
    }

    /**
     * Gets a cursor of this kind of entities that matches the query from the
     * database.
     *
     * @param array $query      mongoDB selection criteria*
     * @return \Mongolid\Cursor\Cursor
     */
    public function where(array $query)
    {
        return $this->productRepository::where($query);
    }

    /**
     * Gets a cursor of this kind of entities from the database.
     *
     * @return \Mongolid\Cursor\Cursor
     */
    public function all()
    {
        return $this->productRepository->all();
    }

    /**
     * Gets a cursor of this kind of entities from the database.
     * @param string $id      mongoDB selection criteria
     * @return ActiveRecord
     */
    public function get(string $id)
    {
        if (is_numeric($id)) {
            return $this->productRepository->where(['lm'=> (int)$id])->first();
        }

        return $this->productRepository::first($id);
    }


    /**
     * Gets a cursor of this kind of entities from the database.
     * @param string $id      mongoDB selection criteria
     * @return bool
     */
    public function updateFromArray(array $dataModel, int $lm) : bool
    {

        $objectId = $this->where(['lm'=> $lm])->first()->_id;

        $productRepository = $this->productRepository::first($objectId);

        foreach ($dataModel as $productKey => $productItem) {
            $productRepository->{$productKey} = $productItem;
        }

        return $productRepository->save();
    }


    /**
     * Gets a cursor of this kind of entities from the database.
     *
     * @return bool
     */
    public function generalAction(ProductModel $productModel, string $action) : bool
    {
        $this->productRepository::getRepositoryFromModel($productModel);
        return $this->productRepository->$action();
    }

}
