<?php

namespace App\Services;

use App\Repositories\HistoryRepository;
use App\Contracts\Services\HistoryServiceInterface;

class HistoryService implements HistoryServiceInterface
{

    /**
     * @var string
     */
    private $historyRepository;

    public function __construct(HistoryRepository $historyRepository)
    {
        $this->historyRepository = $historyRepository;
    }

    /**
     * Gets a cursor of this kind of entities from the database.
     *
     * @return \Mongolid\Cursor\Cursor
     */
    public function getStatusLastImport()
    {
        return $this->historyRepository->all()->sort(['_id'=>-1])->limit(1);
    }
}
