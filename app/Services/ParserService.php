<?php

namespace App\Services;

use Lib\SimpleXLSX;
use App\Models\ProductModel;
use App\Contracts\Services\ParserServiceInterface;

class ParserService implements ParserServiceInterface
{

    public function __construct()
    {
    }

    /**
     * @param string|string $filename
     * @param int|int $headerLine
     * @return array
     * @throws \Exception
     */
    public function parse(string $filename, int $headerLine = 1): array
    {

        if (! file_exists($filename)) {
            throw new \Exception('Open file error.');
        }

        if ($xlsx = SimpleXLSX::parse($filename)) {
            $data = [];

            foreach ($xlsx->rows() as $rowKey => $rowItem) {
                global $header;

                if ($rowKey == $headerLine || $rowKey == 0) {
                    $header = $rowItem;
                    continue;
                }

                $data[] = new ProductModel(array_combine($header, $rowItem));
            }

            return $data;
        }

        throw new \Exception(SimpleXLSX::parse_error());
    }
}
