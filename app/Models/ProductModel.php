<?php
namespace App\Models;

class ProductModel
{

    private $categoryNumber;
    private $lm;
    private $name;
    private $category;
    private $free_shipping;
    private $description;
    private $price;

    public function __construct(array $properties)
    {
        foreach ($properties as $key => $value) {
            $this->{$key} = $value;
        }
    }

    /**
     * @return mixed
     */
    public function getCategoryNumber()
    {
        return $this->categoryNumber;
    }
    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return mixed
     */
    public function getLm()
    {
        return $this->lm;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getFreeShipping()
    {
        return $this->free_shipping;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    public function toArray()
    {
        return call_user_func('get_object_vars', $this);
    }
}
