<?php
namespace App\Model;

class HistoryModel
{

    public $date;

    public $status;

    public $filename;

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @return mixed
     */
    public function getProperties()
    {
        return call_user_func('get_object_vars', $this);
    }
}
