<?php
namespace App\Models;

abstract class BaseModel
{
    public function toArray()
    {
        return call_user_func('get_object_vars', $this);
    }
}
