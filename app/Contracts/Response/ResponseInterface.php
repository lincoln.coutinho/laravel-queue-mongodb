<?php

namespace App\Contracts\Response;

use \Symfony\Component\HttpFoundation\Response as SymfonyResponse;

interface ResponseInterface
{
    /**
     * @param array $data
     * @param int $code
     * @return \Illuminate\Http\Response
     */
    public function responseSuccess(array $data, int $code = SymfonyResponse::HTTP_OK);

    /**
     * Data responseError
     *
     * @param array $data
     * @return \Illuminate\Http\Response
     */
    public function responseError(array $data);

    /**
     * Data responseErrorByException
     *
     * @param \Exception $e
     * @return \Illuminate\Http\Response
     */
    public function responseErrorByException(\Exception $e);
}
