<?php

namespace App\Contracts\Services;

use App\Models\ProductModel;

interface ProductServiceInterface
{

    public function save(ProductModel $productModel) : bool;

    public function insert(ProductModel $productModel): bool;

    public function update(ProductModel $productModel): bool;

    public function delete(ProductModel $productModel): bool;

    public function where(array $query);

    public function all();

    public function updateFromArray(array $dataModel, int $lm) : bool;

    public function get(string $id);

}
