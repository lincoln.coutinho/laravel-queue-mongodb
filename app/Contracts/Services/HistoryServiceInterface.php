<?php

namespace App\Contracts\Services;

interface HistoryServiceInterface
{

    /**
     * Gets a cursor of this kind of entities from the database.
     *
     * @return \Mongolid\Cursor\Cursor
     */
    public function getStatusLastImport();
}
