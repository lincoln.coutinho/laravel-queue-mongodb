<?php

namespace App\Contracts\Services;

interface ParserServiceInterface
{
    /**
     * @param string $filename
     * @param int $headerLine
     * @return array
     */
    public function parse(string $filename, int $headerLine = 1) : array;
}
