<?php

namespace App\Contracts\Repositories;

interface ProductRepositoryInterface extends RepositoryInterface
{
    /**
     * Data import
     *
     * @param array $data
     */
    public function import(array $data);
}
