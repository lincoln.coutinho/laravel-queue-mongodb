<?php

namespace App\GraphQL\Mutation;

use GraphQL;
use App\Contracts\Services\ProductServiceInterface;
use App\Repositories\ProductRepository;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;

class UpdateProductMutation extends Mutation
{
    protected $attributes = [
        'name' => 'updateProduct'
    ];

    /**
     * @var \App\Contracts\Services\ProductServiceInterface
     */
    private $productService;

    public function __construct(ProductServiceInterface $productService)
    {
        $this->productService = $productService;
    }


    public function type()
    {
        return GraphQL::type('Product');
    }

    public function args()
    {
        return [
            'lm' => [
                'name' => 'lm',
                'type' => Type::nonNull(Type::int()),
                'rules' => ['required'],
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required'],
            ],
            'category' => [
                'name' => 'category',
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required'],
            ],
            'free_shipping' => [
                'name' => 'free_shipping',
                'type' => Type::nonNull(Type::int()),
                'rules' => ['required'],
            ],
            'description' => [
                'name' => 'description',
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required'],
            ],
            'price' => [
                'name' => 'price',
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required'],
            ],
        ];
    }

    public function resolve($root, $args)
    {

        $product = $this->productService->get($args['lm']);

        if (!$product instanceof ProductRepository) {
            throw new \Exception('Resource not found');
        }

        $product->lm = $args['lm'];
        $product->name = $args['name'];
        $product->category = $args['category'];
        $product->free_shipping = $args['free_shipping'];
        $product->description = $args['description'];
        $product->price = $args['price'];

        $product->save();
        return $product;
    }
}
