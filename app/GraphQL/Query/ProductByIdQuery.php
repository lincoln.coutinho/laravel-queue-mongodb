<?php

namespace App\GraphQL\Query;

use GraphQL;
use App\Bit;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use App\Contracts\Services\ProductServiceInterface;

class ProductByIdQuery extends Query
{
    protected $attributes = [
        'name' => 'productById'
    ];

    /**
     * @var \App\Contracts\Services\ProductServiceInterface
     */
    private $productService;

    public function __construct(ProductServiceInterface $productService)
    {
        $this->productService = $productService;
    }

    public function type()
    {
        return GraphQL::type('Product');
    }

    public function args()
    {
        return [
            'lm' => [
                'name' => 'lm',
                'type' => Type::nonNull(Type::int()),
                'rules' => ['required']
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $product = $this->productService->get($args['lm']);

        if (!$product instanceof ProductRepository) {
            throw new \Exception('Resource not found');
        }

        return $product;
    }
}
