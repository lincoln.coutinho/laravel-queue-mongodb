<?php

namespace App\GraphQL\Query;

use GraphQL;
use App\Bit;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use App\Contracts\Services\ProductServiceInterface;

class AllProductsQuery extends Query
{
    protected $attributes = [
        'name' => 'allProducts'
    ];

    /**
     * @var \App\Contracts\Services\ProductServiceInterface
     */
    private $productService;

    public function __construct(ProductServiceInterface $productService)
    {
        $this->productService = $productService;
    }

    public function type()
    {
        return Type::listOf(GraphQL::type('Product'));
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        return $this->productService->all();
    }
}
