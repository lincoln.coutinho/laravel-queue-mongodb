<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class ProductType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Product',
        'description' => 'Product'
    ];

    public function fields()
    {

        return [
            'lm' => [
                'name' => 'lm',
                'type' => Type::nonNull(Type::int()),
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string()),
            ],
            'category' => [
                'name' => 'category',
                'type' => Type::nonNull(Type::string()),
            ],
            'free_shipping' => [
                'name' => 'free_shipping',
                'type' => Type::nonNull(Type::int()),
            ],
            'description' => [
                'name' => 'description',
                'type' => Type::nonNull(Type::string()),
            ],
            'price' => [
                'name' => 'price',
                'type' => Type::nonNull(Type::string()),
            ],
        ];
    }
}
