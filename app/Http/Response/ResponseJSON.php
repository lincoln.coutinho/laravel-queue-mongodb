<?php

namespace App\Http\Response;

use App\Contracts\Response\ResponseInterface;
use \Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ResponseJSON implements ResponseInterface
{
    /**
     * Data responseSuccess
     *
     * @param array $data
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseSuccess(array $data, int $code = SymfonyResponse::HTTP_OK)
    {
        return response()->json(['status' => 'OK', 'data' => $data], $code);
    }

    /**
     * Data responseError
     *
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseError(array $data)
    {
        return response()->json($data, SymfonyResponse::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Data responseErrorByException
     *
     * @param \Exception $e
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseErrorByException(\Exception $e)
    {

        return response()->json(['error' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
    }
}
