<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\ProductsConsumerJob;
use App\Contracts\Response\ResponseInterface;
use App\Contracts\Services\ProductServiceInterface;

class ProductController extends Controller
{
    /**
     * @var \App\Contracts\Response\ResponseInterface
     */
    private $response;

    /**
     * @var \App\Contracts\Services\ProductServiceInterface
     */
    private $productService;

    /**
     * @var \Illuminate\Http\Request
     */
    private $request;

    public function __construct(ResponseInterface $response, ProductServiceInterface $productService, Request $request)
    {
        $this->response = $response;
        $this->productService = $productService;
        $this->request = $request;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->response->responseSuccess($this->productService->all()->toArray());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(string $identify)
    {
        $product = $this->productService->get($identify);

        return $this->response->responseSuccess($product->toArray());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $filename = md5(microtime()).'.'.config('parser.extension');

        $this->request->file('file')->storeAs('xlsx', $filename);

        dispatch(new ProductsConsumerJob($filename));

        return $this->response->responseSuccess(['status' => 'OK']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $lm
     * @return \Illuminate\Http\Response
     */
    public function update(int $lm)
    {

        $bodyRequest = json_decode($this->request->getContent(), true);
        $product = $this->productService->updateFromArray($bodyRequest, $lm);

        return $this->response->responseSuccess(['status' => 'OK']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $lm
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $lm)
    {
        $this->productService->get($lm)->delete();

        return $this->response->responseSuccess(['status' => 'OK']);
    }
}
