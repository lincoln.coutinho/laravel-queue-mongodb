<?php

namespace App\Http\Controllers;

use App\Contracts\Response\ResponseInterface;
use App\Contracts\Services\HistoryServiceInterface;

class HistoryController extends Controller
{
    /**
     * @var \App\Contracts\Response\ResponseInterface
     */
    private $response;

    /**
     * @var \App\Contracts\Services\HistoryServiceInterface
     */
    private $historyService;

    public function __construct(ResponseInterface $response, HistoryServiceInterface $historyService)
    {
        $this->response = $response;
        $this->historyService = $historyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->response->responseSuccess($this->historyService->getStatusLastImport()->toArray());
    }
}
