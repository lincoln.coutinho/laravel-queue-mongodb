<?php

namespace App\Http\Middleware;

use Closure;
use JsonSchema\Validator;
use App\Contracts\Response\ResponseInterface;

class JsonSchemaValidator
{

    /**
     * @var \JsonSchema\Validator
     */
    private $validator;

    /**
     * @var \App\Contracts\Response\ResponseInterface
     */
    private $response;

    public function __construct(Validator $validator, ResponseInterface $response)
    {
        $this->validator = $validator;
        $this->response = $response;
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        $data = json_decode($request->getContent());

        $jsonSchema = 'file://'.sprintf(config('validator.schema.path'), 'product.json');

        $this->validator->validate(
            $data,
            (object)['$ref' => $jsonSchema]
        );

        if (!$this->validator->isValid()) {
            $errors =[];
            foreach ($this->validator->getErrors() as $error) {
                $errors[] = $error;
            }

            return $this->response->responseError($errors);
        }

        return $next($request);
    }
}
