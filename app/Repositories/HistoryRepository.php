<?php
namespace App\Repositories;

class HistoryRepository extends \Mongolid\ActiveRecord
{
    /**
     * The database collection used by the model.
     *
     * @var string
     */
    protected $collection = 'history';

    public function insertHistory(string $status, string $filename)
    {

        $historyRepository = new self;

        $historyRepository->status = $status;
        $historyRepository->filename = $filename;
        $historyRepository->dateImport = date("Y-m-d H:i:s");

        $historyRepository->save();

        return $historyRepository;
    }


    public function updateHistory(HistoryRepository $historyRepository)
    {

        $historyRepository->save();

        return $historyRepository;
    }
}
