<?php
namespace App\Repositories;

use App\Models\ProductModel;

class ProductRepository extends \Mongolid\ActiveRecord
{
    /**
     * The database collection used by the model.
     *
     * @var string
     */
    protected $collection = 'products';



    public function import(array $dataProducts) : bool
    {

        foreach ($dataProducts as $product) {

            $productRepository = self::getRepositoryFromModel($product);
            if (!$productRepository->save()) {
                return false;
            }
        }

        return true;
    }

    public static function getRepositoryFromModel(ProductModel $productModel)
    {
        $productRepository = new self;

        $productArray = $productModel->toArray();
        foreach ($productArray as $productKey => $productItem) {
            $productRepository->{$productKey} = $productItem;
        }

        return $productRepository;
    }
}
