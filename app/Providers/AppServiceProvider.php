<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind(
            'App\Contracts\Services\HistoryServiceInterface',
            'App\Services\HistoryService'
        );

        $this->app->bind(
            'App\Contracts\Services\ProductServiceInterface',
            'App\Services\ProductService'
        );

        $this->app->bind(
            'App\Contracts\Response\ResponseInterface',
            'App\Http\Response\ResponseJSON'
        );

        $this->app->bind(
            'App\Contracts\Services\ParserServiceInterface',
            'App\Services\ParserService'
        );
    }
}
