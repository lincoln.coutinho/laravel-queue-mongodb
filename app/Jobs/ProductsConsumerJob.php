<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Repositories\ProductRepository;
use App\Repositories\HistoryRepository;
use App\Contracts\Services\ParserServiceInterface;

class ProductsConsumerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    private $filename;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($filename)
    {
        $this->filename = config('parser.path').$filename;
    }


    /**
     * Execute the job.
     * @param \App\Repositories\ProductRepository $productRepository
     * @param \App\Repositories\HistoryRepository $historyRepository
     * @param \App\Contracts\Services\ParserServiceInterface $parser
     */
    public function handle(ParserServiceInterface $parser, ProductRepository $productRepository, HistoryRepository $historyRepository)
    {
        $products = $parser->parse($this->filename);

        $history = $historyRepository->insertHistory('OK', $this->filename);

        try {
            if (!$productRepository->import($products)) {
                $this->setHistoryError($history);
            }
        } catch (\Exception $e) {
            $this->setHistoryError($history);
        }
    }

    private function setHistoryError(HistoryRepository $historyRepository)
    {
        $historyRepository->status = 'NOK';
        $historyRepository->save();
    }
}
