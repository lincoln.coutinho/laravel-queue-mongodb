<?php

return [

    'path' => storage_path('app/xlsx/'),

    'extension' => 'xlsx',

    'maxsize' => 5 * 1024, // in kB

    'mimes' => [
        'xlsx'
    ],

];
