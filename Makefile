config:
	composer install && cp .env.example .env && php artisan key:generate && chmod -R 0777 /var/www/storage /var/www/bootstrap/cache

test:
	docker exec app /bin/sh -c "vendor/bin/phpunit"

up:
	docker-compose up -d --build

queue-listen:
	docker exec app /bin/sh -c "php artisan queue:listen --queue products"

.PHONY: config test queue-listen up